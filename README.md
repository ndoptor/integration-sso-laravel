<p align="center"><a href="https://n-doptor-portal.nothi.gov.bd/" target="_blank"><img src="https://n-doptor-accounts-stage.nothi.gov.bd/img/doptor.png" width="200"></a></p>

# N-DOPTOR SSO

Single Sign On Laravel Package for N-DOPTOR

## Installation

You can install the package via composer:

```shell
composer require ndoptor/integration-sso-laravel
```

### Laravel 7.x and above

The package will automatically register itself, so you can start using it immediately.

## Configuration

#### After installing the package, need to update or add these lines on `.env` file

-   `LOGIN_SSO=true`
-   `LOGIN_SSO_URL=https://n-doptor-accounts-stage.nothi.gov.bd/login`
-   `LOGOUT_SSO_URL=https://n-doptor-accounts-stage.nothi.gov.bd/logout`
-   `LOGIN_API=https://n-doptor-api-stage.nothi.gov.bd/login`
-   `NDOPTOR_API_URL=https://n-doptor-api-stage.nothi.gov.bd/`

#### Update `web.php` in `routes` directory

-   Remove `Auth::routes();`, if exists.

#### Use `ndoptor.auth` for N-DOPTOR SSO authentication

```
Route::middleware(['ndoptor.auth'])->group(function () {
    /// here your authentication route
});
```

## Uses

-   If you need to extra task after login, you can create a method in `Controller.php` named `afterLogin`
-   Similarly, you can create a method in `Controller.php` named `afterLogout` if you need

## Credits

-   [Tappware Solutions Limited](https://tappware.com)

For any questions, you can reach out to the author of this package, Md. Hasan Sayeed.

Thank you for using it.
