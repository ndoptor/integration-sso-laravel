<?php

/*
|--------------------------------------------------------------------------
| Crud Generator Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/login', 'Ndoptor\SSO\Http\Controllers\NdoptorSSOController@showLoginForm')->name('login');
Route::get('/login-response', 'Ndoptor\SSO\Http\Controllers\NdoptorSSOController@loginResponse')->name('login-response')->middleware('web');
Route::post('/logout', 'Ndoptor\SSO\Http\Controllers\NdoptorSSOController@logout')->name('logout')->middleware('web');