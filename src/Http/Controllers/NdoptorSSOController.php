<?php

namespace Ndoptor\SSO\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Cookie\CookieJar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Http;

class NdoptorSSOController extends Controller
{
    public function showLoginForm(Request $request)
    {
        $redirect_url = url('/login-response');
        if ($request->has('redirect') && $request->get('redirect') != '') {
            $redirect_url .= '?redirect=' . ($request->get('redirect'));
        }
        return redirect(config('ndoptor.login_sso_url') . '?referer=' . base64_encode(url($redirect_url)));
    }

    public function loginResponse(Request $request)
    {
        $data = json_decode(base64_decode($request->data), true);
        if ($data && $data['status'] == 'success' && !empty($data['token'])) {
            $currentDesk = Http::withHeaders(['api-version' => 1])->withToken($data['token'])->post(config('ndoptor.api_url') . '/api/user/me', []);
            if ($currentDesk->json() && $currentDesk->json()['status'] == 'success') {
                $user_info = $currentDesk->json()['data'];
                session()->put(['_ndoptor_loggedin_user_session' => $user_info]);
                session()->save();
                $expires = strtotime('+1 days');
                $this->createNewCookie($data, $expires);
                if (method_exists($this, 'afterLogin')) {
                    $this->afterLogin($request);
                }
                if ($request->has('redirect') && $request->get('redirect') != '') {
                    return redirect($request->get('redirect'));
                } else {
                    return redirect('/');
                }
            } else {
                return redirect('/login');
            }
        } else {
            return redirect('/login');
        }
    }

    public function createNewCookie($cookie_data, $expires)
    {
        $cookie_data = base64_encode(json_encode($cookie_data));
        setcookie('_ndoptor', $cookie_data, $expires, '/');
        return $cookie_data;
    }

    public function logout(Request $request)
    {
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        unset($_COOKIE['_ndoptor']);
        setcookie('_ndoptor', null, strtotime('-1 days'), '/');
        if (method_exists($this, 'afterLogout')) {
            $this->afterLogout($request);
        }
        return redirect(config('ndoptor.logout_sso_url') . '?referer=' . base64_encode(url('/login-response')));
    }
}
