<?php

return [
	/*
    |--------------------------------------------------------------------------
    | Login SSO
    |--------------------------------------------------------------------------
    |
    | This value for SSO enable or disable.
    |
    */
	'login_sso' => env('LOGIN_SSO', true),

	/*
    |--------------------------------------------------------------------------
    | Login SSO URL
    |--------------------------------------------------------------------------
    |
    | This value is the url for sso login authentication.
    |
    */
	'login_sso_url' => env('LOGIN_SSO_URL', 'https://n-doptor-accounts-stage.nothi.gov.bd/login'),

	/*
    |--------------------------------------------------------------------------
    | Logout SSO URL
    |--------------------------------------------------------------------------
    |
    | This value is the url for sso logout authentication.
    |
    */
	'logout_sso_url' => env('LOGOUT_SSO_URL', 'https://n-doptor-accounts-stage.nothi.gov.bd/logout'),

	/*
    |--------------------------------------------------------------------------
    | API URL
    |--------------------------------------------------------------------------
    |
    | This value is the url for api url.
    |
    */
	'api_url' => env('NDOPTOR_API_URL', 'https://n-doptor-api-stage.nothi.gov.bd/'),

];
