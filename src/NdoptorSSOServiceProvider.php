<?php

namespace Ndoptor\SSO;

use Illuminate\Contracts\Http\Kernel;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Ndoptor\SSO\Http\Middleware\NdoptorAuthenticate;

class NdoptorSSOServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
		$this->loadRoutesFrom(__DIR__ . '/routes/web.php');

		$router = $this->app->make(Router::class);
		$router->aliasMiddleware('ndoptor.auth', NdoptorAuthenticate::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
		$this->mergeConfigFrom(
			__DIR__.'/config/ndoptor.php', 'ndoptor'
		);
    }
}
